package hino;

import hino.dto.Champion;
import java.util.Collections;
import java.util.List;

/**
 * Java is <b>pass-by-value</b>, or you might call it <b>pass-by-value-of-the-reference</b>.<br/>
 * <b>Passing by value</b> refers to passing a copy of the value.<br/>
 * <b>Passing by reference</b> refers to passing the real reference of the variable in memory.<br/>
 */
public class ReferenceCheck {

  public static void main(String[] args) {
    List<Champion> champions = Collections.singletonList(Champion.builder().name("test").build());

    Champion test = champions.get(0);
    test.setName("flying-duck");

    System.out.println(champions);
  }
}

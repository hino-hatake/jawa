package hino.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Champion {

  private String name;
  private Integer level;
}

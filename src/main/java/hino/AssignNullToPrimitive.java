package hino;

public class AssignNullToPrimitive {

  public static void main(String[] args) {
    Integer wrapper = null;

    // This unboxing will generate NPE
    System.out.println(wrapper <= 1L);
  }
}

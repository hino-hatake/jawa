package hino;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumingThat;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

/**
 * Unit test for simple HelloWorld.
 */
@Disabled
@Slf4j
public class HelloWorldTest {

  private static final String MOOD = "good";

  /**
   * Rigorous Test :-)
   */
  @Test
  void shouldAnswerWithTrue() {
    assumingThat("good".equals(MOOD), () -> {
      log.info("It's running...");
      assertTrue(true);
    });
  }

  @Test
  void shouldNotRun() {
    assumingThat(!"good".equals(MOOD), () -> {
      log.info("It's running...");
      assertTrue(true);
    });
  }

  @Test
  void letsSeeHowItThrows() {
    // you can ignore the return value if do not wanna perform additional check
    Throwable exception = Assertions.assertThrows(UnsupportedOperationException.class, () -> {
      throw new UnsupportedOperationException("nanii!!!");
    });

    Assertions.assertEquals("nanii!!!", exception.getMessage());
  }
}
